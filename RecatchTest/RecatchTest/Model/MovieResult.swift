//
//  MovieResult.swift
//  RecatchTest
//
//  Created by John Kricorian on 11/06/2018.
//  Copyright © 2018 John Kricorian. All rights reserved.
//

import ObjectMapper

class MovieResult: Mappable {
    
    var results = [Movie]()
    var title = ""
    var releaseDate = ""
    var image = ""
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        results <- map["results"]
        title <- map["title"]
        releaseDate <- map["release_date"]
        image <- map["poster_path"]
    }
}

