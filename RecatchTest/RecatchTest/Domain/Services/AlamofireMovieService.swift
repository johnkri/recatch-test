//
//  AlamofireMovieService.swift
//  RecatchTest
//
//  Created by John Kricorian on 11/06/2018.
//  Copyright © 2018 John Kricorian. All rights reserved.
//

import Foundation

import Alamofire
import AlamofireObjectMapper

class AlamofireMovieService: AlamofireService, MovieService {
    
    func getTopRatedMovies(completion: @escaping MoviesResult) {
        get(at: .topRatedMovies)
            .responseArray(keyPath: "results") { (response: DataResponse<[MovieResult]>) in
                switch response.result {
                case .success:
                    completion(response.result.value ?? [], response.result.error)
                case .failure(let error):
                    print(error.localizedDescription)
                }
        }
    }
    
    func getNowPlayingMovies(completion: @escaping MoviesResult) {
        get(at: .nowPlaying)
            .responseArray(keyPath: "results") { (response: DataResponse<[MovieResult]>) in
                switch response.result {
                case .success:
                    completion(response.result.value ?? [], response.result.error)
                case .failure(let error):
                    print(error.localizedDescription)
                }
        }
    }
    
    func getUpcomingMovies(completion: @escaping MoviesResult) {
        get(at: .upComing)
            .responseArray(keyPath: "results") { (response: DataResponse<[MovieResult]>) in
                switch response.result {
                case .success:
                    completion(response.result.value ?? [], response.result.error)
                case .failure(let error):
                    print(error.localizedDescription)
                }
        }
    }
}
