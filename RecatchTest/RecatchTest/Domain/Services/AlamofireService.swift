//
//  AlamofireService.swift
//  RecatchTest
//
//  Created by John Kricorian on 11/06/2018.
//  Copyright © 2018 John Kricorian. All rights reserved.
//

import Alamofire

class AlamofireService {
    
    var context: ApiContext
    
    init(context: ApiContext) {
        self.context = context
    }
    
    func get(at route: ApiRoute, params: Parameters? = nil) -> DataRequest {
        return request(at: route, method: .get, params: params, encoding: URLEncoding.default)
    }
    
    func post(at route: ApiRoute, params: Parameters? = nil) -> DataRequest {
        return request(at: route, method: .post, params: params, encoding: JSONEncoding.default)
    }
    
    func put(at route: ApiRoute, params: Parameters? = nil) -> DataRequest {
        return request(at: route, method: .put, params: params, encoding: JSONEncoding.default)
    }
    
    func request(at route: ApiRoute, method: HTTPMethod, params: Parameters?, encoding: ParameterEncoding) -> DataRequest {
        let url = route.url(for: context.environment)
        return Alamofire
            .request(url, method: method, parameters: nil, encoding: encoding).validate()
    }
}

