//
//  MovieService.swift
//  RecatchTest
//
//  Created by John Kricorian on 11/06/2018.
//  Copyright © 2018 John Kricorian. All rights reserved.
//

import Foundation

typealias MoviesResult = (_ movies: [MovieResult], _ error: Error?) -> Void

protocol MovieService: class {
    
    func getTopRatedMovies(completion: @escaping MoviesResult)
    func getNowPlayingMovies(completion: @escaping MoviesResult)
    func getUpcomingMovies(completion: @escaping MoviesResult)
}

