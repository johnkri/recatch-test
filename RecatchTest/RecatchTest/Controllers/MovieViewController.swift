//
//  ViewController.swift
//  RecatchTest
//
//  Created by John Kricorian on 11/06/2018.
//  Copyright © 2018 John Kricorian. All rights reserved.
//

import UIKit
import AlamofireImage

class MovieViewController: UIViewController {

    // MARK: - Properties
    var receivedMovies = [MovieResult]()

    // MARK: - Outlets
    @IBOutlet weak var movieSegmentedControl: UISegmentedControl!
    @IBOutlet weak var movieTableView: UITableView!
    
    // MARK: - Actions
    @IBAction func movieSegmentedControlPressed(_ sender: UISegmentedControl) {
        let index = movieSegmentedControl.selectedSegmentIndex
        switch index {
        case 0:
            getNowPlayingMovies()
        case 1:
            getMostPopularMovies()
        case 2:
            getUpComingMovies()
        default:
            break
        }
    }
    
    // MARK: - Internal Methods
    internal func getNowPlayingMovies() {
        let env = ApiEnvironment.production
        let context = NonPersistentApiContext(environment: env)
        let service = AlamofireMovieService(context: context)
        service.getNowPlayingMovies { [weak self] (movies, error) in
            guard let strongSelf = self else {
                return
            }
            guard error == nil else {
                print(error?.localizedDescription ?? "")
                return
            }
            strongSelf.receivedMovies = movies
            strongSelf.movieTableView.reloadData()
        }
    }
    
    internal func getMostPopularMovies() {
        let env = ApiEnvironment.production
        let context = NonPersistentApiContext(environment: env)
        let service = AlamofireMovieService(context: context)
        service.getTopRatedMovies { [weak self] (movies, error) in
            guard let strongSelf = self else {
                return
            }
            guard error == nil else {
                print(error?.localizedDescription ?? "")
                return
            }
            strongSelf.receivedMovies = movies
            strongSelf.movieTableView.reloadData()
        }
    }
    
    internal func getUpComingMovies() {
        let env = ApiEnvironment.production
        let context = NonPersistentApiContext(environment: env)
        let service = AlamofireMovieService(context: context)
        service.getUpcomingMovies { [weak self] (movies, error) in
            guard let strongSelf = self else {
                return
            }
            guard error == nil else {
                print(error?.localizedDescription ?? "")
                return
            }
            strongSelf.receivedMovies = movies
            strongSelf.movieTableView.reloadData()
        }
    }
    
    // MARK: - App LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        getNowPlayingMovies()
    }
}

extension MovieViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return receivedMovies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MovieTableViewCell", for: indexPath) as! MovieTableViewCell
        
        cell.movieTitleLabel.text = receivedMovies[indexPath.row].title
        cell.releaseDateMovieLabel.text = "Release Date: " + receivedMovies[indexPath.row].releaseDate
        if let urlString = URL(string: "https://image.tmdb.org/t/p/w500/\(receivedMovies[indexPath.row].image)") {
            cell.movieImage.af_setImage(withURL: urlString)
        }

        return cell
    }
}

