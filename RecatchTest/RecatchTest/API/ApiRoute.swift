//
//  ApiRoute.swift
//  RecatchTest
//
//  Created by John Kricorian on 11/06/2018.
//  Copyright © 2018 John Kricorian. All rights reserved.
//

import Foundation

enum ApiRoute { case
    
    movie(id: Int),
    nowPlaying,
    topRatedMovies,
    upComing
    
    var path: String {
        switch self {
        case .movie(let id):
            return "movies/\(id)"
        case .topRatedMovies:
            return "top_rated"
        case .nowPlaying:
            return "now_playing"
        case .upComing:
            return "upcoming"
        }
    }
    
    func url(for environment: ApiEnvironment) -> String {
        return "\(environment.url)movie/\(path)/?api_key=\(ApiParameters.apiKey)&language=fr-FR"
    }
}
