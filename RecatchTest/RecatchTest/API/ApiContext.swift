//
//  ApiContext.swift
//  RecatchTest
//
//  Created by John Kricorian on 11/06/2018.
//  Copyright © 2018 John Kricorian. All rights reserved.
//

import Foundation

protocol ApiContext: class {
    
    var environment: ApiEnvironment { get set }
}

