//
//  ApiEnvironment.swift
//  RecatchTest
//
//  Created by John Kricorian on 11/06/2018.
//  Copyright © 2018 John Kricorian. All rights reserved.
//

import Foundation

enum ApiEnvironment: String {
    case production = "https://api.themoviedb.org/3/"
    
    var url: String {
        return rawValue
    }
}
