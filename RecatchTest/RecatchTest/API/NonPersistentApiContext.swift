//
//  NonPersistentApiContext.swift
//  RecatchTest
//
//  Created by John Kricorian on 11/06/2018.
//  Copyright © 2018 John Kricorian. All rights reserved.
//

import Foundation

class NonPersistentApiContext: ApiContext {
    
    init(environment: ApiEnvironment) {
        self.environment = environment
    }
    
    var environment: ApiEnvironment
}

